В двух словах: синхронизирует разницу в атомах на клиенте и сервере, передавая дельту в
обоих направлениях по SockJS.
Т.е. есть один атом на клиенте, а на сервере мапа с копиями этих же атомов, где ключём
является id sockjs-сессии.
Работать удобно по add-watch на изменения каждого атома, что на клиенте, что на сервере.
Кроме того, на сервере add-watch на общую мапу всех состояний позволяет контролировать
подключение и отключение клиентов. По всему дереву можно работать с core.logic.
Пока частично тестированный прототип.

В директории example есть простенький пример.

Пример клиента:
```clojure
...
(:require [urix.web.shared_state :refer [init]])
...

(defn dbg [& x]
  (if (and (.-console js/window) (.-log (.-console js/window)))
    (.log js/console (clojure.string/join "\n" (map pr-str x)))))

(def _state (:state (init))) ;; <- вот состояние
(aset js/window "xx" (fn [a b]
                       (swap! _state assoc a b)))

(aset js/window "xy" (fn []
                       (dbg @_state)))

(add-watch _state :visio (fn [_ st old-state new-state]
                          (aset (-> js/document .-body) "innerHTML" (pr-str new-state))))
```
Пример сервера (все состояния в sts):

```clojure
(require '[urix.web.shared_state :refer [init states] :rename {states sts}])

(defn index-page []
  {:status 200 :headers {"Content-Type" "text/html; charset=UTF-8"}
   :body (html [:html [:head]
                [:body]
                [:script {:src "/js/es5-shim/4.1.14/es5-shim.min.js"}]
                [:script {:src "/js/es5-shim/4.1.14/es5-sham.min.js"}]
                [:script {:src "/js/sockjs-client/1.0.2/sockjs.min.js"}]
                [:script {:src "/client.js"}]])})

(defn http-server []
  (org.httpkit.server/run-server
   (-> (compojure.core/routes
        (init)
        (compojure.core/GET "/" [] (index-page))
        (compojure.route/resources "/js/" {:root "META-INF/resources/webjars/"})
        (compojure.route/resources "/" {:root "public"})
        (compojure.route/not-found "Page not found"))
       wrap-cookies wrap-params) {:port 8080}))
```

(urix.web.shared_state/set-sync-callbacks <id-sockjs-session> (fn [<state-atom> <incoming-data>] ...)

