(defproject test1 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :main test1.core
  :plugins [[lein-cljsbuild "1.1.3"]]
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [rum "0.9.1"]
                 [org.clojure/clojurescript "1.9.89"]
                 [compojure "1.4.0"]
                 [ring "1.4.0"]
                 [org.webjars/es5-shim "4.1.14"]
                 [urix.web.shared_state "0.1.0"]]
  :cljsbuild {:builds [{:id "dev" :source-paths ["cljs"]
                        :compiler {:pretty-print true
                                   :main test1
                                   :output-to "resources/public/client.js"
                                   :optimizations :whitespace}}
                       {:id "prod" :source-paths ["cljs"]
                        :compiler {:pretty-print false
                                   :output-to "resources/public/client.js"
                                   :optimizations :advanced}}]})
