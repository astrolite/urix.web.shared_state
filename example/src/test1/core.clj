(ns test1.core
  (:require org.httpkit.server
            rum.core
            [compojure.core :refer [GET]]
            compojure.route
            [urix.web.shared_state :refer [init states] :rename {states sts}]))

(add-watch sts ::main
           (fn [_ _ old new]
             (prn (keys old)
                  (keys new)
                  (clojure.set/difference (set (keys new)) (set (keys old))))
             (doseq [i (clojure.set/difference (set (keys new)) (set (keys old)))]
               (prn (get @sts i))
               (urix.web.shared_state/set-sync-callbacks
                i (fn [& x] (prn x)))
               (prn "(get @sts i)" (get @sts i) (.getWatches (get @sts i)))
               (swap! (get @sts i) assoc :text "wwww")
               (Thread/sleep 3000)
               (swap! (get @sts i) merge {:text "wwww3" :pos 1})
               )))

(defonce http-server (atom nil))

(defn -main []
  (if @http-server (@http-server))
  (reset! http-server
          (org.httpkit.server/run-server
           (compojure.core/routes
            (init)
            (GET "/" req (rum.core/render-html
                          [:html [:head [:meta {:http-equiv "Content-Type"
                                                :content "text/html;charset=UTF-8"}]
                                  [:title "тест 1"]]
                           [:body]
                           [:script {:src "/js/es5-shim/4.1.14/es5-shim.min.js"}]
                           [:script {:src "/js/es5-shim/4.1.14/es5-sham.min.js"}]
                           [:script {:src "/js/sockjs-client/1.0.2/sockjs.min.js"}]
                           [:script {:src "/client.js"}]]))
            (GET "/favicon.ico" req "")
            (compojure.route/resources "/js/" {:root "META-INF/resources/webjars/"})
            (compojure.route/resources "/" {:root "public"}))
           {:port 8080})))
