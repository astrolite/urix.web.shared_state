(ns test1
  (:require [rum.core :as rum]
            [urix.web.shared_state :refer [init]]))

(defonce state (:state (init)))

(def input_dom (atom nil))

(defn set_point [& {component :comp data :data}]
  (when (and component (not @input_dom))
    (reset! input_dom component))
  (when @input_dom
    (js/console.log (js/ReactDOM.findDOMNode @input_dom))
    (when (and (map? data) (:pos data))
      (-> input_dom deref js/ReactDOM.findDOMNode .focus)
      (-> input_dom deref js/ReactDOM.findDOMNode (.setSelectionRange (:pos data) (:pos data))))
    ))

(rum/defc x < {:did-mount (fn [st]
                            (js/console.log "on-mount")
                            (set_point :comp (-> st :rum/react-component))
                            )} [data]
  (js/console.log "on-update")
  (set_point :data data)
  [:input {:on-change (fn [e] (swap! state assoc :text (.-value (.-target e))))
            :value (:text data)}])

(rum/defc root [data]
  [:div {:class "label"} (pr-str (-> data (dissoc :to_eval)))
   [:br]
   (x data)
   [:br]
   (map (fn [i] [:div i]) (range 10)) [:hr]])

(add-watch state ::render
           (fn [_ _ _ data]
             (rum/mount (root data) js/document.body)))

(aset js/window "xx" (fn [x] (reset! state x)))
(aset js/window "xy" (fn [] (js/console.log (pr-str @state))))

