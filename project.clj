(defproject urix.web.shared_state "0.1.0"
  :description "urix.web.shared_state"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  ;;:aot :all
  :source-paths ["cljc"]
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [timothypratley/patchin "0.3.5"]
                 [org.clojure/core.match "0.3.0-alpha4"]
                 [org.webjars/sockjs-client "1.0.2"]
                 [com.cognitect/transit-clj "0.8.285"]
                 [com.cognitect/transit-cljs "0.8.237"]
                 [http-kit-sockjs "0.2.0"]])
