(ns urix.web.shared_state
  (:require
   #?(:clj  [clojure.core.match :refer [match]]
      :cljs [cljs.core.match :refer-macros [match]])
   #?(:clj [methojure.sockjs.session :refer :all])
   #?(:clj [methojure.sockjs.core :refer :all])
   #?(:clj [cognitect.transit :as transit])
   [cognitect.transit :as transit]
   [timothypratley.patchin :as patchin]))

#?(:clj
   (def sync-callbacks (atom {})))

(defn make-shared-state [& {:keys [state watcher-name sender sess-id]
                            :or {watcher-name :shared-state-sync}}]
  (let [allow-to-send-sync-data (atom 1)
        change-state (fn [diff]
                       (reset! allow-to-send-sync-data nil)
                       (swap! state patchin/patch diff)
                       (reset! allow-to-send-sync-data 1))]
    (add-watch state watcher-name (fn [_ _ old-state new-state]
                                    (when @allow-to-send-sync-data
                                      (sender {:patch (patchin/diff old-state new-state)}))))
    (fn [incoming-data]
      (let [_
            (match incoming-data
                   {:init init-value} (reset! state init-value)
                   {:patch diff} #?(:clj  (locking state (change-state diff))
                                    :cljs (change-state diff)))
            callback #?(:clj (get @sync-callbacks sess-id) :cljs nil)]
        (when callback
          (callback state incoming-data))))))

#?(:cljs (defn init [& {:keys [url] :or {url "/sockjs"} :as args}]
           (let [sj (js/SockJS. url)
                 snd (fn [data]
                       (let [w (transit/writer :json)]
                         (.send sj (transit/write w data))))
                 state-sync-callback (atom nil)
                 state (get args :state (atom nil))
                 ;; don't forget to implement:
                 ;; http://stackoverflow.com/questions/18930786/sockjs-client-auto-reconnect
                 _ (aset sj
                         "onopen"
                         (fn []
                           (reset! state-sync-callback (make-shared-state :state state :sender snd))
                           (aset sj
                                 "onmessage"
                                 #(let [data (transit/read (transit/reader :json) (-> % .-data))]
                                    (@state-sync-callback data)))
                           #_(snd {:init @state})))
                 _ (aset (js* "window") "sj" sj) ;; advanced mode hack
                 ]
             {:sj sj :snd snd :state state})))

;; я не знаю, как с methojure.sockjs рабоать в функциональном стиле,
;; потому получилось такое убожество

#?(:clj
   (defn read-transit [s]
     (-> s
         .getBytes
         java.io.ByteArrayInputStream.
         (transit/reader :json)
         transit/read)))

#?(:clj
   (defn write-transit [d]
     (let [out (java.io.ByteArrayOutputStream.)
           writer (transit/writer out :json)]
       (transit/write writer d)
       (.toString out))))

#?(:clj
   (def states (atom nil)))

#?(:clj
   (def state-sync-callbacks (atom {})))
#?(:clj (defn set-sync-callbacks [sess-id func]
          (swap! sync-callbacks assoc sess-id func)))
#?(:clj
   (def snd (fn [sj data] (send! sj {:type :msg :content (write-transit data)}))))
#?(:clj
   (def on-sj-message (fn [session msg]
                        ((get @state-sync-callbacks (get session :id)) (-> msg read-transit)))))
#?(:clj
   (def on-sj-open (fn [this session]
                     (let [sess-id (get session :id)
                           new-state (atom nil)]
                       (swap! state-sync-callbacks
                              assoc sess-id (make-shared-state :state new-state
                                                               :sess-id sess-id
                                                               :sender #(snd session %)))
                       (swap! states assoc sess-id new-state)
                       ))))
#?(:clj
   (def on-sj-close (fn [this session]
                      (swap! states dissoc (get session :id))
                      (swap! sync-callbacks dissoc (get session :id))
                      (swap! state-sync-callbacks dissoc (get session :id)))))

#?(:clj
   (defrecord SJC []
     SockjsConnection
     (on-open [this session] (on-sj-open this session) session)
     (on-message [this session msg] (on-sj-message session msg) session)
     (on-close [this session] (on-sj-close this session) session)))

#?(:clj
   (defn init [& {:keys [url] :or {url "/sockjs"} :as args}]
     (sockjs-handler url (->SJC) {:response-limit 4096})))

